// scope

let x1 =12
var x2 =13
x3 = 14
const x4 =15 // const ari rogorc let ubralod mnishvnelobas ver ucvli

x1 =22
x2=23
x3=24
// x4=25 //ver icvleba radgan const ari


function scope_1(){
    console.log(x1)
    console.log(x2)
    console.log(x3)
    console.log(x4)
}
scope_1()

function scope_2(){
    let y1=3
    if (4<9){
        let y2 =4
        console.log(y1)
        console.log(y2)
    }
    // console.log(y2) ar daibechdeba radgan let ari lokaluri funqcia
}

scope_2()

function scope_3(){
    var z1=3
    if (4<9){
        var z2 =4
        console.log(z1)
        console.log(z2)
    }
     console.log(z2) // daiwereba radgan vars viyenebt
}

// scope_3()

function scope_4(){
    console.log(z1) 
}
// scope_4()

function scope_5(){
     q1=3
    if (4<9){
         q2 =4
        console.log(q1)
        console.log(q2)
    }
     console.log(q2) 
}
scope_5()

function scope_6(){
    console.log(q1)
    console.log(q2)

}
scope_6() //arafers tu ar uwer win mashin yvela funqciashi shegilia gamoyeneba